<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 21.02.2019
 * Time: 00:46
 */

use Symfony\Component\Console\Command\Command;
use Rfd\ImageMagick\ImageMagick;
use Rfd\ImageMagick\CLI\Operation\Factory as OperationFactory;
use Rfd\ImageMagick\Image\File;
use Rfd\ImageMagick\Options\CommonOptions;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ProcessImageCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('process-image')
            ->setDescription('Process Images for JuZe Fotoadmin')

            ->addArgument('dir', InputArgument::REQUIRED, 'The working directory')

            ->addOption('ext', 'e', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'List of regexp extensions to check', ['jpe?g', 'png'])

            ->addOption('width', '', InputOption::VALUE_REQUIRED, 'Resize width', 1920)
            ->addOption('height', '', InputOption::VALUE_REQUIRED, 'Resize height', 1080)
            ->addOption('no-strip', '', InputOption::VALUE_NONE, 'Whether to strip the image meta tags');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get cli args
        $args = $input->getArguments();
        $options = $input->getOptions();

        // check dir
        $dir = realpath($args['dir']);
        if (! $dir) {
            $output->writeln("<error>Directory \"$args[dir]\" doesn't exist.</error>");
            return;
        }

        // read files
        $extOption = $options['ext'];
        $exts = implode('|', $extOption);
        $files = array_filter(scandir($dir), function ($file) use ($dir, $exts) {
            $path = "$dir/$file";
            $info = pathinfo($path);
            $ext  = $info['extension'] ?? '';

            return preg_match("/($exts)/i", $ext);
        });
        $numberFiles = count($files);

        if ($numberFiles === 0) {
            $output->writeln('<error>No images found to process.</error>');
            return;
        }

        // ask for confirmation
        $questionHelper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            "<info>$numberFiles images to be worked on in directory \"$dir\".</info>\n<question>Continue? [Y/N]</question> ",
            false
        );
        if (! $questionHelper->ask($input, $output, $question)) {
            return;
        }

        // create $im
        $im = new ImageMagick(new OperationFactory());

        // start progress bar
        $progressBar = new ProgressBar($output, $numberFiles);
        $progressBar->start();

        $widthOption = $options['width'];
        $heightOption = $options['height'];
        $stripOption = $options['no-strip'];

        foreach($files as $file) {
            $filepath = "$dir/$file";
            $filename = pathinfo($file, PATHINFO_FILENAME);

            $inputImage = new File($filepath);
            $outputImage = new File("$dir/$filename.jpg");

            $operationBuilder = $im->getOperationBuilder($inputImage);

            $operationBuilder
                ->resize()
                ->setWidth($widthOption)
                ->setHeight($heightOption)
                ->setMode(CommonOptions::MODE_ONLY_SHRINK_LARGER)
                ->setGravity(CommonOptions::GRAVITY_CENTER)
                ->next();

            if ($stripOption) {
                $operationBuilder->strip();
            }

            $operationBuilder
                ->convert(CommonOptions::FORMAT_JPG)
                ->finish($outputImage);

            $progressBar->advance();
        }

        $progressBar->finish();
        $output->writeln('');

        $output->writeln('<info>Done!</info>');
    }
}
